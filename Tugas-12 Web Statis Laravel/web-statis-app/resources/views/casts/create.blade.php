@extends('layouts.master')
@section('title', 'Tambah Pemeran Film')
@section('content')
    <a class="btn btn-info btn-sm mb-3" href="{{ url('cast') }}">Kembali</a>
    <form action="{{ url('cast') }}" method="POST">
        @csrf
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama">
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" id="umur" name="umur">
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" id="bio" rows="5" name="bio"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection
