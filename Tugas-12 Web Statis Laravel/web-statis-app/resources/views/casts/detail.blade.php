@extends('layouts.master')
@section('title', 'Detail Pemeran Film')
@section('content')
    <a class="btn btn-info btn-sm mb-3" href="{{ url('cast') }}">Kembali ke Halaman utama</a>
    <h5>Pemeran no. {{ $casts->id }}</h5>
    <h1> Nama :{{ $casts->nama }}</h1>
    <h3>Umur : {{ $casts->umur }} Tahun</h3>
    <h3>Bio</h3>
    <p>{{ $casts->bio }}</p>
@endsection
