@extends('layouts.master')
@section('title', 'Pemeran Film')
@section('content')
    <a class="btn btn-success" href="{{ url('cast/create') }}">Tambah Pemeran</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($casts as $cast)
                <tr>
                    <th scope="row">{{ $cast->id }}</th scope="row">
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>
                        <form action="{{ url("cast/$cast->id") }}"" method="POST">
                            <a class="btn btn-info btn-sm" href="{{ url("cast/$cast->id") }}">Detail</a>
                            <a class="btn btn-info btn-sm" href="{{ url("cast/$cast->id/edit") }}">Edit</a>
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                        </form>
                    </td>
                </tr>
            @empty
                <p>No users</p>
            @endforelse
        </tbody>
    </table>

@endsection
