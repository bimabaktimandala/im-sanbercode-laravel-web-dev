@extends('layouts.master')
@section('title','Daftar Akun Baru')
@section('content')
<h1>Buat Account Baru!</h1>
<a class="btn btn-primary" href="{{ url('/') }}" role="button">Kembali ke halaman utama</a>
<h3>Sign Up Form</h3>
<form action="/send" method="POST">
    @csrf
    <label for="first-name">First Name :</label><br><br>
    <input type="text" name="first-name" id="first-name"><br><br>
    <label for="last-name">last Name :</label><br><br>
    <input type="text" name="last-name" id="last-name"><br><br>
    <p>Gender :</p>
    <input type="radio" name="gender" id="male" value="male">
    <label for="male">Male</label><br>
    <input type="radio" name="gender" id="female" value="female">
    <label for="female">Female</label><br>
    <input type="radio" name="gender" id="other" value="other">
    <label for="other">Other</label><br><br>
    <label for="nationality">Nationality</label>
    <select name="nationality" id="nationality">
    <option value="Indonesia">Indonesian</option>
    <option value="Singapura">Singaporean</option>
    <option value="Singapura">Malaysian</option>
    </select><br>
    <p>Language Spoken</p>
    <input type="checkbox" name="spoke" id="idn" value="idn">
    <label for="idn">Bahasa Indonesia</label><br>
    <input type="checkbox" name="spoke" id="eng" value="eng">
    <label for="eng">English</label><br>
    <input type="checkbox" name="spoke" id="other" value="other">
    <label for="other">Other</label><br><br>
    <label for="bio">Bio :</label><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    <button type="submit" class="btn btn-primary">Sign Up</button>
</form>
@endsection
    