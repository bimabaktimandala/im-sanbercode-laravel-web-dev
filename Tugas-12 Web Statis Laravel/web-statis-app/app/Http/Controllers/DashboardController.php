<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index () {
        return view('pages.index');
    }


    public function register () {
        return view('pages.register');
    }

    public function send (Request $request) {
        
        $namaDepan = $request->input('first-name');
        $namaBelakang = $request->input('last-name');
        return view('.pages.welcome',['depan' => $namaDepan,'belakang' => $namaBelakang]);
    }

    public function table () {
        return view('pages.table');
    }

    public function dataTable () {
        return view('pages.dataTable');
    }
}
