<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();

        return view('casts.index', ['casts' => $casts]);
    }

    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:6',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        /* menyimpan ke db menggunakan query builder */
        DB::table('casts')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),

        ]);
        return redirect('cast');
    }
    public function show($id)
    {
        $casts = DB::table('casts')->find($id);

        return view('casts.detail', ['casts' => $casts]);
    }
    public function edit($id)
    {
        $casts = DB::table('casts')->find($id);
        return view('casts.edit', ['casts' => $casts]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|min:6',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);
        DB::table('casts')
            ->where('id', $id)
            ->update([
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio'),
            ]);
        return redirect("cast/$id");
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();
        return redirect("cast");
    }
}
