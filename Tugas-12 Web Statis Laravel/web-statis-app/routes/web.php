<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
/* dashboard controller */
Route::get('/', [DashboardController::class, 'index']);
Route::get('/register', [DashboardController::class, 'register']);
Route::post('/send', [DashboardController::class, 'send']);
Route::get('/welcome', [DashboardController::class, 'welcome']);
Route::get('/table', [DashboardController::class, 'table']);
Route::get('/data-table', [DashboardController::class, 'dataTable']);

/* cast controller */
Route::get('/cast', [CastController::class, 'index']); /* menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card) */
Route::get('/cast/create', [CastController::class, 'create']); /* menampilkan form untuk membuat data pemain film baru */
Route::post('/cast', [CastController::class, 'store']); /* menyimpan data baru ke tabel Cast */
Route::get('/cast/{cast_id}', [CastController::class, 'show']); /* menampilkan detail data pemain film dengan id tertentu */
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']); /* menampilkan form untuk edit pemain film dengan id tertentu */
Route::put('/cast/{cast_id}', [CastController::class, 'update']); /* menyimpan perubahan data pemain film (update) untuk id tertentu */
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']); /* menghapus data pemain film dengan id tertentu */